<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="nama"> <br> <br>
        <label>Last name:</label> <br><br>
        <input type="text" name="nama"> <br> <br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gender" > Male <br>
        <input type="radio" name="gender" > Female <br>
        <input type="radio" name="gender" > Other <br> <br>
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Australia">Australia</option>
            <option value="China">China</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="Bahasa Indonesia"> Bahasa Indonesia <br>
        <input type="checkbox" name="English"> English <br>
        <input type="checkbox" name="Other"> Other <br> <br>
        <label>Bio:</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>
        <input type="Submit" value="Sign Up">
    </form>
</body>
</html>