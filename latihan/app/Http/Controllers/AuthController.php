<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendaftaran (){
        return view('form');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $nama = $request["nama"];

        return view('masuk', compact("nama"));
    }
}
